package com.company.validationlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidationLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ValidationLibraryApplication.class, args);
    }

}
